import Vue from 'vue';
import VueSVGCustomIcon from 'vue-svg-custom-icon';
import App from './App.vue';

import router from './router/router';
import store from './store/store';

Vue.config.productionTip = false;
Vue.use(VueSVGCustomIcon);

new Vue({
  name: 'Task Manager',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
